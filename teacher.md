# 教師用マニュアル

## 授業用のgitリポジトリの作成

### リポジトリのfork

1. <https://gitlab.com> にアカウントがなければ，アカウントを作成する．以下アカウント名を `xxxxx` とする．
2. <https://gitlab.com> にログインする．
3. <https://gitlab.com/ku-istc/gitpod-test> を開き，右上の「Fork」ボタンを押し，自分のアカウントを選択する．
4. forkされた自分のリポジトリが開く．
   - "Settings" / "General" / "Visibility, project features, permissions" の右のExpandをクリックして，
     "Project visivility"を"Public"にし，"Save Changes"ボタンをクリックする．
   - リポジトリの設定をpublicにしないと，学生がアクセスできない．

### Gitpodを開く

1. 自分のリポジトリの表示画面で，"Gitpod"ボタンをクリックする．
   - もしポップアップウィドウが表示されたら"Enable Gitpod"をクリックし，もう一度"Gitpod"ボタンをクリックする．
2. もし，以下のボタンが表示されたらクリックする．
   - "Authorize"ボタン (GitLabの画面)
   - "Accept Terms"ボタン (Gitpodの画面)
   - "Grant Access"ボタン (Gitpodの画面)
3. Gitpodのworkspaceが表示される．

### プログラムの修正とpush

1. Gitpodでプログラムを修正する．
2. Visual Studio Codeと同様の操作で自分のGitLabリポジトリに修正をpushする．
