# README

<img src="gitpod.png" alt="gitpod" width="40%"/>

## Gitpodの利用 (学生用)

1. <https://gitlab.com> にアカウントがなければ，アカウントを作成する．
2. <https://gitpod.io/#https://gitlab.com/ku-istc/gitpod-test> を開き，GitLabのアカウントでログインする．
3. しばらく待つ．
4. 画面下側のターミナルウィンドウに以下を入力すると，"Hello Kobe!"が表示されるはず．
   ```
   make
   ./hellokobe
   ```
5. 2回目以降は <https://gitpod.io> を開き，GitLabのアカウントでログインし，
   gitpod-testのworkspaceをOpenあるいはStartする．
6. workspaceに作成したファイルは，2週間以内に再度workspaceを開けば残っている．

### 利用上の注意

- 50時間/月までは無料
- IDEはVisual Studio Code互換
- gitpod.io 上でLinuxコンテナ (workspace)が起動されており，
  そのターミナル画面が画面下部に表示されている．
  そのコンテナを利用してプログラムのコンパイル・実行，
  ファイルの編集・保存，プログラムのインストールなどが可能
- workspaceを保存してダウンロードすることも可能

### リンク

- <https://gitlab.com>
    - <https://docs.gitlab.com/ee/README.html>
- <https://gitpod.io>
    - <https://www.gitpod.io/docs/>
- <https://kobeucsenshu.github.io/ideinfo/> (システム情報学研究科鎌田先生作成)


## Gitpodの利用 (教師用)

- [教師用マニュアル](teacher.md)を参照
